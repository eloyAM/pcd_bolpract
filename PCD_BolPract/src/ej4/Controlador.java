package ej4;

import messagepassing.MailBox;

public class Controlador extends Thread {
	// No se necesita lista de espera por nivel ya que no habr� m�s de uno
	// esperando, si llega alguien con el mismo nivel se formar� pareja
	private int     tablaNivelId[];	// Indexada por nivel guarda id jugadores
	// Buz�n en el que recibe la info de los jugadores
	private MailBox buzRecJugadores;
	// Indexado por id, un buz�n por jugador para mandarle su pareja
	private MailBox buzSendPareja[];
	private int     nivelMax;
	
	public Controlador(int numNiveles, MailBox buzonConexion, MailBox buzonParejas[]) {
		this.nivelMax = numNiveles-1;
		this.buzSendPareja   = buzonParejas;
		this.buzRecJugadores = buzonConexion;
		tablaNivelId = new int[numNiveles];
		for (int i = 0; i < tablaNivelId.length; i++) {
			tablaNivelId[i] = -1;
		}
	}
	
	public void run() {
		while (true) {
			/* esperar a recibir info de alg�n jugador */
			InfoJugador infoJugador = (InfoJugador) buzRecJugadores.receive();
			InfoJugador infoPareja  = posiblePareja(infoJugador.getNivel());
			/* si se puede formar pareja */
			if (infoPareja != null) {
				/* enviar pareja a cada jugador */
				buzSendPareja[infoJugador.getIdentificador()].send(infoPareja);
				buzSendPareja[infoPareja.getIdentificador()].send(infoJugador);
				/* eliminar de jugadores esperando */
				tablaNivelId[infoPareja.getNivel()] = -1;
			}
			/* si no */
			else {
				/* a�adir a jugadores esperando */
				tablaNivelId[infoJugador.getNivel()] = infoJugador.getIdentificador();
			}	
				
		}
		
	}
	
	// Elegir pareja con hasta dos niveles de diferencia
	// Preferir la menor diferencia de nivel
	private InfoJugador posiblePareja(int nivelJugador) {
		// Primero buscamos alguien del mismo nivel
		if(tablaNivelId[nivelJugador] != -1)
			return new InfoJugador(tablaNivelId[nivelJugador], nivelJugador);
		else
		// Despu�s buscamos seg�n los casos
		if (nivelJugador > 1  &&  nivelJugador < nivelMax-1) {
			// Repetir para una diferencia de dos niveles
			for (int i = 1; i <= 2; i++) {	
				if(tablaNivelId[nivelJugador-i] != -1)	// Un nivel inferior
					return new InfoJugador(tablaNivelId[nivelJugador-i], nivelJugador-i);
				if(tablaNivelId[nivelJugador+i] != -1)	// Un nivel superior
					return new InfoJugador(tablaNivelId[nivelJugador+i], nivelJugador+i);
			}
		} else
		
		if (nivelJugador == 1) {
			if(tablaNivelId[0] != -1)	// Un nivel inferior
				return new InfoJugador(tablaNivelId[0], 0);
			if(tablaNivelId[2] != -1)	// Un nivel superior
				return new InfoJugador(tablaNivelId[2], 2);
			if(tablaNivelId[3] != -1)	// Dos niveles superior
				return new InfoJugador(tablaNivelId[3], 3);
		} else
		
		if(nivelJugador == 0) {
			if(tablaNivelId[1] != -1)	// Un nivel superior
				return new InfoJugador(tablaNivelId[1], 1);
			if(tablaNivelId[2] != -1)	// Dos niveles superior
				return new InfoJugador(tablaNivelId[2], 2);
		} else
		
		if(nivelJugador == nivelMax-1) {
			if(tablaNivelId[nivelMax-2] != -1)	// Un nivel inferior
				return new InfoJugador(tablaNivelId[nivelMax-2], nivelMax-2);
			if(tablaNivelId[nivelMax] != -1)	// Un nivel superior
				return new InfoJugador(tablaNivelId[nivelMax], nivelMax);
			if(tablaNivelId[nivelMax-3] != -1)	// Un nivel inferior
				return new InfoJugador(tablaNivelId[nivelMax-3], nivelMax-3);
		} else
		
		if(nivelJugador == nivelMax) {
			if(tablaNivelId[nivelMax-1] != -1)	// Un nivel inferior
				return new InfoJugador(tablaNivelId[nivelMax-1], nivelMax-1);
			if(tablaNivelId[nivelMax-2] != -1)	// Dos niveles inferior
				return new InfoJugador(tablaNivelId[nivelMax-2], nivelMax-2);
		}
		
		// Si no se ha encontrado pareja devolver null para indicarlo
		return null;
	}
}
