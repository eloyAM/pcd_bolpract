package ej4;

import messagepassing.MailBox;

// Encargado de sincronizar dos hilos para que ninguno
// vuelva a ejecutarse sin que el otro haya finalizado
public class Sincronizador extends Thread {
	// Buz�n en el que recibe la info (qui�n termina)
	private MailBox buzSync;
	// Indexado por id. Un buz�n por hilo para desbloquearlo
	private MailBox buzDesblSync[];

	public Sincronizador(MailBox buzonSincronizacion, MailBox buzonDesbloqueoSincronizacion[]) {
		this.buzSync = buzonSincronizacion;
		this.buzDesblSync = buzonDesbloqueoSincronizacion;
	}

	public void run() {
		while (true) {
			// recibir ID de qui�n termin�
			int idPareja = (int) buzSync.receive();
			buzDesblSync[idPareja].send(null);
		}
	}
}
