package ej4;

import java.util.Random;
import messagepassing.MailBox;

public class Jugador extends Thread {
	private int id;			// Identificador �nico
	private int nivel;		// Nivel comenzando en cero
	// Buz�n compartido para informar al controlador de nuestra existencia
	private MailBox buzConexion;	
	// Buz�n propio en el que recibimos la info de nuestra pareja
	private MailBox buzRecPareja;	
	// Buz�n compartido para impresi�n por pantalla en exclusi�n mutua
	private MailBox buzPrintMutex;	
	// Buz�n compartido para informar de que ya hemos imprimido
	private MailBox buzSync; 		
	// Buz�n propio para sincronizarnos con nuestra pareja
	private MailBox buzDesblSync; 	
	private Random  rnd; 			// Generador de niveles
	private InfoJugador infoPareja; // Pareja que el controlador nos asigna

	// Genera un nivel aleatorio entre 0 y el m�ximo-1
	private void generarNivel() {
		nivel = rnd.nextInt(Ej4Main.NUM_NIVELES);
	}

	// Imprime el id y nivel propio y de la pareja
	private void imprimirInfo() {
		System.out.println("-Jugador " + id + " con NIVEL " + nivel);
		System.out.println(" Juega con jugador "+infoPareja.getIdentificador()
						+ " con NIVEL " + infoPareja.getNivel());
		try {
			Thread.sleep((int) Math.random() * 10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Jugador(int id, MailBox buzonConexion, MailBox buzonRecibirPareja, MailBox buzonImprimirMutex,
			MailBox buzonSincronizacion, MailBox buzonDesbloqueoSincronizacion) {
		this.id = id;
		this.rnd = new Random();
		this.nivel = -1;
		this.infoPareja = null;
		this.buzSync       = buzonSincronizacion;
		this.buzConexion   = buzonConexion;
		this.buzRecPareja  = buzonRecibirPareja;
		this.buzDesblSync  = buzonDesbloqueoSincronizacion;
		this.buzPrintMutex = buzonImprimirMutex;
	}

	public void run() {
		while (true) {
			generarNivel();
			// Enviar al controlador nuestra info
			buzConexion.send(new InfoJugador(id, nivel));
			// Esperar hasta que haya alguien de nivel similar
			infoPareja = (InfoJugador) buzRecPareja.receive();	
			buzPrintMutex.receive(); //Conseguir exclusi�n mutua para imprimir
			imprimirInfo();			  //Imprimir datos nuestros y de la pareja
			buzPrintMutex.send(null); //Liberar exclusi�n mutua para imprimir
			// Informar para que nuestra pareja sepa que hemos imprimido
			buzSync.send(infoPareja.getIdentificador());
			// Esperar hasta que nuestra pareja haya imprimido tambi�n
			buzDesblSync.receive(); 
		}
	}

}
