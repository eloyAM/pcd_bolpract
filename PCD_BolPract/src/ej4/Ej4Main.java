package ej4;

import java.util.ArrayList;
import messagepassing.MailBox;

public class Ej4Main {
	public static final int NUM_NIVELES   = 10;
	public static final int NUM_JUGADORES = 20;
	
	public static void main(String[] args) {
		// Donde los jugadores mandan su info
		MailBox buzonConexion       = new MailBox();
		// Imprimir en exclusi�n mutua
		MailBox buzonImprimirMutex  = new MailBox();
		// Donde los jugadores informan que imprimieron
		MailBox buzonSincronizacion = new MailBox();
		// Donde los jugadores reciben su pareja
		MailBox buzonParejas[] = new MailBox[NUM_JUGADORES];
		// Donde los jugadores esperan a que su pareja haya impreso tambi�n
		MailBox buzonDesbloqueoSincronizacion[] = new MailBox[NUM_JUGADORES];
		
		// El controlador se encarga de formar parejas
		Controlador   controlador   = new Controlador(NUM_NIVELES, buzonConexion, buzonParejas);
		// El impresor habilita la exclusi�n mutua para que los hilos impriman
		ImprimirMutex impresor      = new ImprimirMutex(buzonImprimirMutex);
		// El sincronizador hace de puente entre dos jugadores para desbloquearse al finalizar
		Sincronizador sincronizador = new Sincronizador(buzonSincronizacion, buzonDesbloqueoSincronizacion);
		
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>(NUM_JUGADORES);
		
		inicializarBuzones(buzonParejas, buzonDesbloqueoSincronizacion);

		inicializarJugadores(buzonConexion, buzonImprimirMutex, buzonSincronizacion, buzonParejas,
				buzonDesbloqueoSincronizacion, jugadores);
		
		System.out.println("COMIENZO");
		/* lanzar hilos */
		jugadores.forEach(t -> t.start());
		controlador.start();
		impresor.start();
		sincronizador.start();
		/* detener manualmente, se ejecutan 4ever & ever */
	}

	// Inicializar jugadores asignando buzones correspondientes
	private static void inicializarJugadores(MailBox buzonConexion, MailBox buzonImprimirMutex,
			MailBox buzonSincronizacion, MailBox[] buzonParejas, MailBox[] buzonDesbloqueoSincronizacion,
			ArrayList<Jugador> jugadores) {
		for (int id = 0; id < NUM_JUGADORES; id++) {
			Jugador jugador = new Jugador(id,
					buzonConexion, buzonParejas[id],
					buzonImprimirMutex,
					buzonSincronizacion, buzonDesbloqueoSincronizacion[id]);
			jugadores.add(jugador);
		}
	}

	// Inicializar arrays de buzones
	private static void inicializarBuzones(MailBox[] buzonParejas, MailBox[] buzonDesbloqueoSincronizacion) {
		for (int i = 0; i < NUM_JUGADORES; i++) {
			buzonParejas[i] = new MailBox();
			buzonDesbloqueoSincronizacion[i] = new MailBox();
		}
	}
}
