package ej4;

import java.io.Serializable;

// Se usar� como mensaje que env�an los jugadores al controlador
// para infomar de sus caracter�sticas
public class InfoJugador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3491875332776965381L;
	private int identificador;
	private int nivel;

	public InfoJugador(int id, int nivel) {
		this.identificador = id;
		this.nivel = nivel;
	}

	public int getIdentificador() {
		return identificador;
	}

	public int getNivel() {
		return nivel;
	}
}
