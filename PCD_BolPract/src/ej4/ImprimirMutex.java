package ej4;

import messagepassing.MailBox;

// La impresi�n la realizan realmente los hilos
// esta clase se encarga de habilitar la exclusi�n mutua para ello
// como si de un sem�foro binario se tratase
public class ImprimirMutex extends Thread {
	private MailBox buzon;

	public ImprimirMutex(MailBox buzon) {
		this.buzon = buzon;
	}

	public void run() {
		// Inicializar para que de primeras se pueda tener acceso
		// despu�s ser�n los hilos quienes se pasen la mutex
		buzon.send(null);
	}

}
