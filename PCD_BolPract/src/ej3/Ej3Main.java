package ej3;

import java.util.ArrayList;
import java.util.List;

public class Ej3Main {
	public static final int NUM_OPS          = 3;
	public static final int NUM_HILOS        = 12;
	public static final int NUM_HILOS_POR_OP = 4;

	public static void main(String[] args) {
		ArrayList<Hilo> hilos        = new ArrayList<Hilo>(NUM_HILOS);
		Secuenciador    secuenciador = new Secuenciador(NUM_OPS);
		MonitorImpMutex impresor     = new MonitorImpMutex();

		inicializarHilos(hilos, secuenciador, impresor);

		hilos.forEach(t -> t.start());
	}

	private static void inicializarHilos(List<Hilo> hilos, Secuenciador secuenciador, MonitorImpMutex impresor) {
		int id = 0;
		for (int i = 0; i < NUM_HILOS_POR_OP; i++) {
			for (int operacion = 0; operacion < NUM_OPS; operacion++) {
				Hilo hilo = new Hilo(id++, operacion, secuenciador, impresor);
				hilos.add(hilo);
			}
		}
	}

}
