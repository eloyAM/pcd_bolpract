package ej3;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Monitor que organiza la ejecución de los hilos de manera
// que las operaciones de las que dispone se ejecuten en orden
public class Secuenciador {
	private ReentrantLock l;
	private Condition condiciones[];
	private int numOps;
	private int turno;
	
	public Secuenciador(int numeroDeOperaciones) {
		this.numOps = numeroDeOperaciones;
		l = new ReentrantLock();
		condiciones = new Condition[numeroDeOperaciones];
		for (int i = 0; i < condiciones.length; i++) {
			condiciones[i] = l.newCondition();
		}
		turno = 0;
	}
	
	// Un hilo pedirá la ejeución de la operación correspondiente
	public void Operacion(int operacion) {
		l.lock();
		try {
			// Mientras no sea el turno de la operación esperar
			// en la condición (cola) correspondiente
			while (turno != operacion) {
				try {
					condiciones[operacion].await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// Al acabar ceder el turno a la operación siguiente
			turno = (operacion+1)%numOps;
			// y despertar a alguien de la operación siguiente
			condiciones[turno].signal();
		} finally {
			l.unlock();
		}
	}

	
}
