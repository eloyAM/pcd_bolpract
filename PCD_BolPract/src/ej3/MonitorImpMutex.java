package ej3;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Monitor que emula un cerrojo o sem�foro binario para
// conseguir exclusi�n mutua y que cada hilo imprima
public class MonitorImpMutex {
	private ReentrantLock l;
	private Condition cond;
	private boolean isPantallaDisponible;

	public MonitorImpMutex() {
		l = new ReentrantLock();
		cond = l.newCondition();
		isPantallaDisponible = true;
	}
	
	// acquire/lock/wait
	public void obtenerMutexImprimir() {
		l.lock();
		try {
			while (!isPantallaDisponible) {
				try {
					cond.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			isPantallaDisponible = false;
		} finally {
			l.unlock();
		}
	}

	// release/unlock/signal
	public void liberarMutexImprimir() {
		l.lock();
		try {
			isPantallaDisponible = true;
			cond.signal();
		} finally {
			l.unlock();
		}
	}

}
