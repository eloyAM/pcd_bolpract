package ej3;

// Hilo que invoca la operaci�n correspondiente del monitor secuenciador
public class Hilo extends Thread {
	private Secuenciador secuenciador;
	private MonitorImpMutex impresor;
	private int operacion;
	private int id;

	public Hilo(int id, int operacion, Secuenciador secuenciador, MonitorImpMutex impresor) {
		this.secuenciador = secuenciador;
		this.operacion = operacion;
		this.impresor = impresor;
		this.id = id;
	}

	// Imprime que se inicia, ejecuta la operaci�n e indica que termin�
	public void run() {
		for (int i = 0; i < 3; i++) {
			imprimirInicio();
			secuenciador.Operacion(operacion);
			imprimirFin();
		}
	}

	// Imprimir su intenci�n de querer ejecutar la operaci�n
	private void imprimirInicio() {
		impresor.obtenerMutexImprimir();
		System.out.println("-Hilo con ID " + id);
		int opDesdeUno = operacion + 1;
		System.out.println(" Ejecuta operaci�n " + opDesdeUno);
		impresor.liberarMutexImprimir();
	}

	// Imprimir una vez ejecutada la operaci�n
	private void imprimirFin() {
		impresor.obtenerMutexImprimir();
		System.out.println("+Fin hilo ID " + id);
		impresor.liberarMutexImprimir();
	}
}
