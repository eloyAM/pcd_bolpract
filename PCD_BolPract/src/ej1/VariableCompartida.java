package ej1;

import java.util.concurrent.locks.ReentrantLock;

// Variable compartida entre hilos que guardar� la suma total
// Nos ofrece incrementar su valor en exclusi�n mutua
public class VariableCompartida {
	private int var;
	private ReentrantLock l;
	
	public VariableCompartida(){
		var = 0;
		l   = new ReentrantLock();
	}
	
	public int getVar(){
		return var;
	}
	
	public void incrementar(int cantidad){
		l.lock();
		try {
			var += cantidad;
		} finally {
			l.unlock();
		}
	}

}
