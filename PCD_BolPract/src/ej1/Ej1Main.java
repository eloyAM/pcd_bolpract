package ej1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class Ej1Main {
	private static final int NUM_HILOS = 20;
	private static final int VAL_POR_HILO = 250;
	private static final int TAMANO_ARRAY = NUM_HILOS*VAL_POR_HILO;	//5000
	
	public static int rndNums[] = new int[TAMANO_ARRAY];
	public static VariableCompartida sumaTotal  = new VariableCompartida();
	public static ReentrantLock cerrojoImprimir = new ReentrantLock();
	
	public static void main(String[] args) {
		// Inicializar y mostrar array de n�meros aleatorios
		inicializarArray(rndNums);
		imprimirArray(rndNums);
		
		// Crear hilos
		ArrayList<Thread> hilosSumadores = new ArrayList<Thread>(NUM_HILOS);
		inicializarHilos(hilosSumadores);
		
		// Lanzar hilos
		hilosSumadores.forEach(t -> t.start());
		
		// Esperar finalizaci�n de los hilos
		hilosSumadores.forEach(t -> {
			try { t.join(); }
			catch (InterruptedException e) { e.printStackTrace(); }
		});
		
		// Imprimir suma almacenada en la variable compartida
		System.out.println("Suma total de valores pares: "+sumaTotal.getVar());
//		imprimirValorEsperado();
	}

	// Imprime el array en filas de 20 columnas
	private static void imprimirArray(int[] rndNums) {
		int numPorLinea = 20; int numLineaActual = 1;
		System.out.println("Array");
		for (int i = 0; i < rndNums.length; i++) {
			if (numLineaActual == numPorLinea) {
				System.out.println(rndNums[i]);
				numLineaActual = 1;
			} else {
				System.out.print(rndNums[i] + " ");
				numLineaActual++;
			}
		}
	}
	// Rellena el array con valores aleatorios entre 1 y 9
	// as� es m�s c�modo (uniforme) visualizarlo por pantalla
	private static void inicializarArray(int[] rndNums) {
		Random r = new Random();
		for (int i = 0; i < rndNums.length; i++) {
			rndNums[i] = r.nextInt(9) + 1;
		}
	}
	// Inicializa los hilos asignando un id y un rango para acceder al array
	private static void inicializarHilos(List<Thread> hilos) {
		int offset = 0;
		for (int id = 0; id < NUM_HILOS; id++) {
			hilos.add(new HiloSumadorPares(id, offset, offset+VAL_POR_HILO));
			offset += VAL_POR_HILO;
		}
	}
	// Para comprobar la correci�n del programa
	@SuppressWarnings("unused")
	private static void imprimirValorEsperado() {
		int sumaEsperada = 0;
		for (int i = 0; i < rndNums.length; i++) {
			if (rndNums[i]%2 == 0) {
				sumaEsperada += rndNums[i];
			}
		}
		System.out.println("Valor esperado: " + sumaEsperada);
	}
}
