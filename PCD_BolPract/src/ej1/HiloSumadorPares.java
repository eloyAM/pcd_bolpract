package ej1;

import java.util.Random;

public class HiloSumadorPares extends Thread {
	private int codigo; 	// Identificador
	private int principio; 	// �ndice el primer elemento
	private int fin; 		// �ndice del *pen�ltimo* elemento
	// Suma de los 250 valores pares asignados al hilo
	private int sumaParcial;

	HiloSumadorPares(int codigo, int principio, int fin) {
		this.codigo = codigo;
		this.principio = principio;
		this.fin = fin;
		sumaParcial = 0;
	}

	public void run() {
		contarValoresPares();
		imprimirInfo();
	}
	
	// Cuenta los valores pares de la parte del array asignado y los a�ade
	// a la suma total compartida en varias pasadas
	private void contarValoresPares() {
		// Inicializar valores para el recorrido
		int valoresATratar = fin - principio; // 250
		int offset = principio;
		int valoresRestantes = valoresATratar;
		Random r = new Random();

		// MIENTRAS queden enteros por analizar de los 250 asignados:
		do {
			int sumaEstaPasada = 0;
			// [0, valoresRestantes <= 250]
			int valoresAnalizarEstaPasada = r.nextInt(valoresRestantes + 1);
			// Analizar X valores no tratados todav�a
			for (int i = offset; i < offset+valoresAnalizarEstaPasada; i++) {
				// Contar s�lo n�meros pares
				if (Ej1Main.rndNums[i] % 2 == 0)
					sumaEstaPasada += Ej1Main.rndNums[i];
			}
			// A�adir suma de los X valores pares a la suma parcial del hilo
			sumaParcial += sumaEstaPasada;
			// Incrementar variable compartida entre hilos (SECCI�N CR�TICA)
			Ej1Main.sumaTotal.incrementar(sumaEstaPasada);
			// Ajustar n�mero de valores restantes por tratar
			valoresRestantes -= valoresAnalizarEstaPasada;
			// Ajustar �ndice para recorrido
			offset += valoresAnalizarEstaPasada;
		} while (valoresRestantes > 0);
	}

	private void imprimirInfo() {
		Ej1Main.cerrojoImprimir.lock();
		try {
			// Imprimir c�digo y suma parcial del hilo (SECCI�N CR�TICA)
			System.out.println("-Hilo ID " + codigo);
			System.out.println(" Suma parcial = " + sumaParcial);
			System.out.println(" Fin hilo ID " + codigo);
		} finally {
			Ej1Main.cerrojoImprimir.unlock();
		}
	}

	public int getCodigo() {
		return codigo;
	}

	public int getSumaParcial() {
		return sumaParcial;
	}
}
