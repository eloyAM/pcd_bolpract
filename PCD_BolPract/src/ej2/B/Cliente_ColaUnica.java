package ej2.B;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Semaphore;

public class Cliente_ColaUnica extends Thread {
	private int  id;
	private int  tiempoCompra;
	private int  tiempoCaja;
	private long tiempoEsperaCola;
	private Cola_ColaUnica cola;
	private Caja_ColaUnica caja;
	private Semaphore semImprimir;

	public Cliente_ColaUnica(int id, int tiempoCompra, int tiempoCaja, Cola_ColaUnica cola, Semaphore semImprimir) {
		this.id           = id;
		this.tiempoCompra = tiempoCompra;
		this.tiempoCaja   = tiempoCaja;
		this.cola         = cola;
		this.semImprimir  = semImprimir;
		caja			  = null;
	}

	public void run() {
		Instant inicio = Instant.now();
		// Esperamos hasta que haya alguna caja libre
		cola.waitSemaforo();
		Instant fin = Instant.now();
		// Nos posicionamos en alguna de las cajas libres
		caja = cola.entrarUnaCajaLibre();
		try {
			Thread.sleep(tiempoCaja);	// Pasamos los productos
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Indicamos que dejamos la caja
		cola.dejarCajaLibre(caja.getId());
		cola.signalSemaforo();	// Dejar caja libre, pasa el siguiente
		// Vemos cu�nto tiempo estuvo esperando a que le tocase pasar por caja
		Duration diferenciaTiempo = Duration.between(inicio, fin);
		tiempoEsperaCola = diferenciaTiempo.toMillis();
		imprimirInfoHilo();
	}
	
	
	private void imprimirInfoHilo() {
		// Protocolo de entrada
		try {
			semImprimir.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Secci�n cr�tica
		System.out.println("- Cliente "+id+" atendido en caja "+caja.getId());
		System.out.println("Tiempo de compra " + tiempoCompra);
		System.out.println("Tiempo en caja " + tiempoCaja);
		System.out.println("Tiempo de espera en la cola de caja "+tiempoEsperaCola);
		// Protocolo de salida
		semImprimir.release();
	}
	
	public long getTiempoEsperaCola() {
		return tiempoEsperaCola;
	}
	
	public int getTiempoCompra() {
		return tiempoCompra;
	}
}
