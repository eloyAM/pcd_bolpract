package ej2.B;

// En este caso el sem�foro lo contiene la cola
// Posee un atributo porque el sem�foro nos dice que hay
// alguna caja libre pero luego tenemos que ver cu�l
public class Caja_ColaUnica {
	private int id;
	private boolean isLibre;

	public Caja_ColaUnica(int id) {
		this.id      = id;
		this.isLibre = true;
	}

	public int getId() {
		return id;
	}

	public boolean isLibre() {
		return isLibre;
	}

	public void setLibre(boolean isLibre) {
		this.isLibre = isLibre;
	}
}
