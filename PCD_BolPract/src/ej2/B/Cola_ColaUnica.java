package ej2.B;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Cola_ColaUnica {
	// Sem�foro general para indicar que hay alguna caja libre
	private Semaphore semaforo;
	// Sem�foro binario para cambiar la disponibilidad en exclusi�n mutua
	private Semaphore semEstadoCajas;
	// Cajas manejadas por la cola �nica
	private ArrayList<Caja_ColaUnica> cajas;
	
	public Cola_ColaUnica(int numCajas) {
		semaforo = new Semaphore(numCajas);
		cajas    = new ArrayList<Caja_ColaUnica>(numCajas);
		semEstadoCajas = new Semaphore(1);
		for (int id = 0; id < numCajas; id++) {
			cajas.add(new Caja_ColaUnica(id));
		}
	}
	
	// Wrapper para la operaci�n wait
	public void waitSemaforo() {
		try {
			semaforo.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// Wrapper para la operaci�n signal
	public void signalSemaforo() {
		semaforo.release();
	}
	
	// Precond: llamar solo cuando sepamos que al menos hay una caja libre
	// Busca una caja libre en exclusi�n mutua y la marca como ocupada
	public Caja_ColaUnica entrarUnaCajaLibre() {
		try {
			semEstadoCajas.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Caja_ColaUnica caja = null;
		for (int i = 0; i < cajas.size(); i++) {
			if (cajas.get(i).isLibre()) {
				caja = cajas.get(i);
				break;
			}
		}
		caja.setLibre(false);
		semEstadoCajas.release();
		return caja;
	}
	
	// Marca como libre la caja indicada en exclusi�n mutua
	public void dejarCajaLibre(int id) {
		try {
			semEstadoCajas.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Caja_ColaUnica caja = cajas.get(id);
		caja.setLibre(true);
		semEstadoCajas.release();
	}
}
