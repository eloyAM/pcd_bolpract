package ej2.B;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Ej2Main_ColaUnica {
	public static final int NUM_CLIENTES = 50;
	public static final int NUM_CAJAS    = 10;

	public static void main(String[] args) {
		Semaphore semImprimir = new Semaphore(1);
		Cola_ColaUnica cola = new Cola_ColaUnica(NUM_CAJAS);
		ArrayList<Cliente_ColaUnica> clientes = new ArrayList<Cliente_ColaUnica>(NUM_CLIENTES);

		// Inicializar
		inicializarClientes(semImprimir, cola, clientes);
		
		
		// Lanzar hilos seg�n tiempo de compra
		clientes.stream()
			.sorted(Comparator.comparing(Cliente_ColaUnica::getTiempoCompra))
			.forEach(t -> t.start());

		clientes.forEach(t -> {
			try { t.join(); } 
			catch (InterruptedException e) { e.printStackTrace(); }
		});
		
		// Obtener tiempo medio de espera en la cola
		Double media = clientes.stream()
								.mapToLong(t -> t.getTiempoEsperaCola())
								.average()
								.orElse(0.0);

		System.out.println("Tiempo medio de espera (milisec): " + media);

	}
	
	// Asigna aleat a cada cliente tiempos de compra y de caja
	private static void inicializarClientes(Semaphore semImprimir, Cola_ColaUnica cola,
			List<Cliente_ColaUnica> clientes) {
		Random r = new Random(1);
		for (int id = 0; id < NUM_CLIENTES; id++) {
			int tiempoCompra = r.nextInt(500) + 1;
			int tiempoCaja = r.nextInt(500) + 1;
			Cliente_ColaUnica cli = new Cliente_ColaUnica(id, tiempoCompra, tiempoCaja, cola, semImprimir);
			clientes.add(cli);
		}
	}

}
