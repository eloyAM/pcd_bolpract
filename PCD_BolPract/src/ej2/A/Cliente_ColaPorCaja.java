package ej2.A;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Semaphore;

public class Cliente_ColaPorCaja extends Thread {
	private int  id;
	private int  tiempoCompra;
	private int  tiempoCaja;
	private long tiempoEsperaCola;
	private Caja_ColaPorCaja caja;
	private Semaphore semImprimir;

	public Cliente_ColaPorCaja(int id, int tiempoCompra, int tiempoCaja, Caja_ColaPorCaja caja, Semaphore semImprimir) {
		this.id           = id;
		this.tiempoCompra = tiempoCompra;
		this.tiempoCaja   = tiempoCaja;
		this.caja         = caja;
		this.semImprimir  = semImprimir;
	}


	public void run() {
		Instant inicio = Instant.now();
		caja.waitSemaforo();	// Esperamos en la caja asignada
		Instant fin = Instant.now();
		try {
			Thread.sleep(tiempoCaja);	// Pasamos los productos
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		caja.signalSemaforo();	// Dejar caja libre, pasa el siguiente
		// Vemos cu�nto tiempo estuvo esperando a que le tocase pasar por caja
		Duration diferenciaTiempo = Duration.between(inicio, fin);
		tiempoEsperaCola = diferenciaTiempo.toMillis();
		imprimirInfoHilo();
	}


	private void imprimirInfoHilo() {
		// Protocolo de entrada
		try {
			semImprimir.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Secci�n cr�tica
		System.out.println("- Cliente " +id+ " atendido en caja "+caja.getId());
		System.out.println("Tiempo de compra " + tiempoCompra);
		System.out.println("Tiempo en caja " + tiempoCaja);
		System.out.println("Tiempo de espera en la cola de caja "+tiempoEsperaCola);
		// Protocolo de salida
		semImprimir.release();
	}

	public long getTiempoEsperaCola() {
		return tiempoEsperaCola;
	}
	
	public int getTiempoCompra() {
		return tiempoCompra;
	}
}
