package ej2.A;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Ej2Main_ColaPorCaja {
	public static final int NUM_CLIENTES = 50;
	public static final int NUM_CAJAS    = 10;

	public static void main(String[] args) {
		Semaphore semImprimir = new Semaphore(1);
		ArrayList<Caja_ColaPorCaja> cajas = new ArrayList<Caja_ColaPorCaja>(NUM_CAJAS);
		ArrayList<Cliente_ColaPorCaja> clientes = new ArrayList<Cliente_ColaPorCaja>(NUM_CLIENTES);

		// Inicializar
		inicializarCajas(cajas);
		inicializarClientes(cajas, clientes, semImprimir);
		
		// Lanzar hilos seg�n tiempo de compra
		clientes.stream()
			.sorted(Comparator.comparing(Cliente_ColaPorCaja::getTiempoCompra))
			.forEach(t -> t.start());
		
		clientes.forEach(t -> {
			try { t.join(); } 
			catch (InterruptedException e) { e.printStackTrace(); }
		});
		
		// Obtener tiempo medio de espera en las colas
		Double media = clientes.stream()
								.mapToLong(t -> t.getTiempoEsperaCola())
								.average()
								.orElse(0.0);
		
		System.out.println("Tiempo medio de espera (milisec): " + media);

	}

	private static void inicializarCajas(List<Caja_ColaPorCaja> cajas) {
		for (int id = 0; id < NUM_CAJAS; id++) {
			Caja_ColaPorCaja caja = new Caja_ColaPorCaja(id);
			cajas.add(caja);
		}
	}
	
	// Asigna aleat a cada cliente una caja y tiempos de compra y de caja
	private static void inicializarClientes(List<Caja_ColaPorCaja> cajas,
			List<Cliente_ColaPorCaja> clientes, Semaphore semImprimir) {
		Random r = new Random(1);
		Random randomCajas = new Random(1);
		for (int id = 0; id < NUM_CLIENTES; id++) {
			int indiceCajaAleatorio = randomCajas.nextInt(NUM_CAJAS);
			Caja_ColaPorCaja cajaAleatoria = cajas.get(indiceCajaAleatorio);
			int tiempoCompra = r.nextInt(500) + 1;
			int tiempoCaja = r.nextInt(500) + 1;
			Cliente_ColaPorCaja cli = new Cliente_ColaPorCaja(id, tiempoCompra, tiempoCaja, cajaAleatoria, semImprimir);
			clientes.add(cli);
		}
	}
}
