package ej2.A;

import java.util.concurrent.Semaphore;

// Contiene un sem�foro binario para esperar
public class Caja_ColaPorCaja {
	private int id;
	private Semaphore semaforo;
	
	public Caja_ColaPorCaja(int id) {
		this.id  = id;
		semaforo = new Semaphore(1);
	}
	
	// Wrapper para la operaci�n wait
	public void waitSemaforo() {
		try {
			semaforo.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// Wrapper para la operaci�n signal
	public void signalSemaforo() {
		semaforo.release();
	}
	
	public int getId() {
		return id;
	}
}
